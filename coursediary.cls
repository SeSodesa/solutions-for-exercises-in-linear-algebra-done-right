%% coursediary.cls
%
% Copyright © 2022 Santtu Söderholm
%
% This work may be distributed and/or modified under the conditions of the
% LaTeX Project Public License, either version 1.3 of this license or (at your
% option) any later version. The latest version of this license is in
%
%   http://www.latex-project.org/lppl.txt
%
% and version 1.3 or later is part of all distributions of LaTeX version
% 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Santtu Söderholm
% <santtu.soderholm@tuni.fi>.
%
% This work consists of the files coursediary.cls.

% LaTeX format and class setting

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{coursediary}[2022/06/19 Course diary]

% Pass known options to article class and packages. Ignore unknown options.

\DeclareOption{suomi}{\PassOptionsToPackage{finnish}{babel}}

\DeclareOption{english}{\PassOptionsToPackage{\CurrentOption}{babel}}

\DeclareOption*{%
	\ClassWarning{tau-fys-oplab-kirjoituspohja}{Tuntematon asetus \CurrentOption}%
	\ClassWarning{tau-fys-oplab-kirjoituspohja}{Unknown setting \CurrentOption}%
}

\ProcessOptions\relax

% Set PDF version before loading article class

\RequirePackage{ifluatex}

\ifluatex

	% Set PDF version with LuaLaTeX

	\directlua{
		if pdf.getminorversion() \string~=7 then
			if (status.pdf_gone and status.pdf_gone > 0)
			or (status.pdf_ptr and status.pdf_ptr > 0) then
				tex.error("PDF version cannot be changed anymore")
			else
				pdf.setminorversion(7)
			end
		end
	}
\else
	\pdfminorversion=7
\fi

% Load the base class of this class

\LoadClass[a4paper,12pt]{report}

% Set font after loading article class

\ifluatex

	\RequirePackage{luacode}
	\RequirePackage{fourier}
	\RequirePackage[no-math]{fontspec}
	\fontspec{Erewhon}

\else

	\RequirePackage[T1]{fontenc}
	\RequirePackage[utf8]{inputenc}
	\RequirePackage{fourier}

\fi

% Set language

\RequirePackage{babel}

% Load packages

\RequirePackage[nohead]{geometry}
\RequirePackage{fancyhdr}
\RequirePackage{mathtools}
\RequirePackage{amsfonts}
\RequirePackage{scalerel}
\RequirePackage{pdfpages} % Adding pdf pages to our document
\RequirePackage{enumitem}
\RequirePackage{tikz}
\RequirePackage[labelfont=bf]{caption}
\RequirePackage{tabularx}
\RequirePackage{booktabs}
\RequirePackage{multirow}
\RequirePackage{parskip}
\RequirePackage{xparse}
\RequirePackage[backend=biber, style=ieee]{biblatex}
\addbibresource{sources.bib}
\RequirePackage{csquotes}

%% Set page styles

\pagestyle{plain}

% Delimiters

\DeclarePairedDelimiter\arcs{(}{)}
\DeclarePairedDelimiter\set{\{}{\}}
\DeclarePairedDelimiter\waves{\{}{\}}
\DeclarePairedDelimiter\squares{[}{]}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}

\DeclarePairedDelimiter\kaaret{(}{)}
\DeclarePairedDelimiter\joukko{\{}{\}}
\DeclarePairedDelimiter\aallot{\{}{\}}
\DeclarePairedDelimiter\haat{[}{]}
\DeclarePairedDelimiter\itseis{\lvert}{\rvert}

% Mathematics columns for tables

\newcolumntype{L}{>{\(}l<{\)}}
\newcolumntype{C}{>{\(}c<{\)}}
\newcolumntype{R}{>{\(}r<{\)}}

% Environments

% Figure environments

\NewDocumentEnvironment{kuva}{O{\textwidth}mm}{%
	\begin{minipage}[t]{#1}%
	\centering%
}{%
	\captionsetup{type=figure}%
	\captionof{figure}{#2}%
	\label{#3}%
	\end{minipage}%
}

\RenewDocumentEnvironment{figure}{O{\textwidth}mm}{%
	\begin{minipage}[t]{#1}%
	\centering%
}{%
	\captionsetup{type=figure}%
	\captionof{figure}{#2}%
	\label{#3}%
	\end{minipage}%
}

% Table environments

\NewDocumentEnvironment{taulukko}{O{\textwidth}mm}{%
	\begin{minipage}[b]{#1}%
	\centering%
	\captionsetup{type=table}%
	\captionof{table}{#2}%
	\label{#3}%
}{%
	\end{minipage}%
}

\RenewDocumentEnvironment{table}{O{\textwidth}mm}{%
	\begin{taulukko}[#1]{#2}{#3}%
}{%
	\end{taulukko}%
}

% Allows align to extend past page limits

\allowdisplaybreaks

% Instatiate document information

\title\mytitle
\author{\authorone\and\authortwo}
\date\today

% Archivable PDF/A

\begin{filecontents}[overwrite]{\jobname.xmpdata}
	\Title{\mytitle}
	\Author{\myauthor}
	\Subject{\mysubject}
	\Publisher{\mypublisher}
	\Keywords{\mykeywords}
\end{filecontents}

\RequirePackage[a-2b,mathxmp,pdf17]{pdfx}
\hypersetup{pdfstartview=,unicode=true,hidelinks}
\RequirePackage{url}

% Produces intentionally annoying black boxes if lines are overfull.

% \overfullrule=5pt
