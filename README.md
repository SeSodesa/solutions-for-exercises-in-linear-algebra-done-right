# Solutions for exercises in *Linear Algebra Done Right*

This repository contains attempted solutions for exercises in Axler's book
*Linear Algebra Done Right*, that focuses on a determinant-free approach
to studying linear algebra.

## Compiling the document

Install a LaTeX distribution such as [TeX Live](https://tug.org/texlive/),
add its binaries to the `PATH` variable of your operating system so your
shell can locate them, navigate to the folder `tex` and run the command
```sh
pdflatex main.tex && biber main && pdflatex main.tex .
```
The document should now compile as intended.
